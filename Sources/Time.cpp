#include "../Headers/Time.hpp"
using namespace std;

Time::Time(unsigned int H, unsigned int M, unsigned int S, float MS):
  Hour(H), Min(M), Sec(S), MSec(MS) {
    if (MS > 99)
    {
      MSec = MS/1000;
    }
    else if (MS > 9)
    {
      MSec = MS/100;
    }
    else
    {
      MSec = MS/10;
    }
}

Time::Time(unsigned int H, unsigned int M, unsigned int S):
  Hour(H), Min(M), Sec(S), MSec(0) {
}

Time::Time(unsigned int H, unsigned int M):
  Hour(H), Min(M), Sec(0), MSec(0) {
}

Time::Time(unsigned int H):
  Hour(H), Min(0), Sec(0), MSec(0) {
}

Time::Time():
  Hour(0), Min(0), Sec(0), MSec(0) {
}

Time::~Time() {

}

string WZero(unsigned int WOZero, bool isMS) {
  if (!isMS)
    return (WOZero<10) ? ("0" + to_string(WOZero)) : (to_string(WOZero));
  else {
    if (WOZero < 100 && WOZero >= 10)
    {
      return ("0" + to_string(WOZero));
    }
    else
      return (WOZero<10) ? ("00" + to_string(WOZero)) : (to_string(WOZero));
    
  }
}

string Time::getTime() {
  string temp = "";
  temp += 
    WZero(this->Hour, false)  + ":" +
    WZero(this->Min, false)    + ":" +
    WZero(this->Sec, false)    + "." +
    WZero(this->MSec, true);
  return temp;
}

int Time::getHour() {
  return this->Hour;
}

int Time::getMin() {
  return this->Min;
}

int Time::getSec() {
  return this->Sec;
}

float Time::getMSec() {
  return this->MSec;
}