#include "../Headers/Subtitle.hpp"
using namespace std;

Subtitle::Subtitle():SubtitleDelay(0) {

}

Subtitle::~Subtitle() {

}

void Subtitle::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Subtitle::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

void Subtitle::Do (Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-i \"" + this->inputVideo.getVideoFullName() + "\" -i \"" +
  this->SubtitleName + "\" -map 0 -map 1 -c copy -preset veryfast "
  + this->outputVideo.getVideoFullName();
  this->setCommand(str);
}

void Subtitle::setSubtitle(string s) {
  this->SubtitleName = s;
}

void Subtitle::setSubtitleDelay(double d) {
  ifstream checkfile;
  checkfile.open(this->SubtitleName, ios::app);
  if (checkfile.is_open() && this->SubtitleDelay)
  {
    string command = "mv " + this->SubtitleName + " temp.srt";
    cout << command << endl;
    const char* comm = command.c_str();
    system(comm);
    command = "ffmpeg -itsoffset " + to_string(this->SubtitleDelay) + " -i temp.srt " + this->SubtitleName;
    cout << command << endl;
    comm = command.c_str();
    system(comm);
    system("rm temp.srt");
    this->SubtitleDelay = d;
  }
}

void Subtitle::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Subtitle::getEditCommand() {
  return this->command.getCommand();
}
