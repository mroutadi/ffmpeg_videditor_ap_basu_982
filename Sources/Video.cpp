#include "../Headers/Video.hpp"
using namespace std;

Video::Video():VideoName(""), VideoFormat(""){

}

Video::Video(string I):VideoName(""), VideoFormat(""){
  setVideoFullName(I);
}

Video::~Video() {
  
}

void Video::setVideoFullName(string I) {
  this->setVideoName(I.substr(0, I.find_last_of(".")));
  this->setVideoFormat(I.substr(I.find_last_of(".")+1));
}

void Video::setVideoName(string I) {
  this->VideoName = I;
}

void Video::setVideoFormat(string I) {
  this->VideoFormat = I;
}

string Video::getVideoFullName() {
  return this->VideoName + "." + this->VideoFormat;
}

string Video::getVideoName() {
  return this->VideoName;
}

string Video::getVideoFormat() {
  return this->VideoFormat;
}
