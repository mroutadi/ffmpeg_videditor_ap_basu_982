#include "../Headers/Trim.hpp"
using namespace std;

string GetStdoutFromCommand(string cmd) {
  string data;
  FILE * stream;
  const int max_buffer = 256;
  char buffer[max_buffer];
  cmd.append(" 2>&1");

  stream = popen(cmd.c_str(), "r");
  if (stream) {
    while (!feof(stream))
      if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
    pclose(stream);
  }
  return data;
}

Trim::Trim() {

}

Time secToTime2(double Seconds) {
  int H, M, S, MS;
  H = Seconds / 3600;
  Seconds -= H * 3600;
  M = Seconds / 60;
  Seconds -= M * 60;
  S = Seconds;
  Seconds -= S;
  MS = Seconds * 1000;
  return Time(H, M, S, MS);
}

float Timetosec3(Time t) {
  return (t.getHour()*3600 + t.getMin()*60 + t.getSec() + t.getMSec());
}

void Trim::setStartTime(Time S) {
  this->StartTime = S;
}

void Trim::setEndTime(Time S) {
  this->EndTime = S;
}

void Trim::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;
}

void Trim::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

string getTrueValueOfSeconds3(float f) {
  string temp = to_string(f);
  
  if ((int(f*1000) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-3));    
  }
  else if ((int(f*100) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-4));    
  }
  else if ((int(f*10) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-5));    
  }
  else
  {
    return (temp.substr(0, temp.length()-7));    
  }
}

void Trim::Do(Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-i \"" + this->inputVideo.getVideoFullName() + "\" -filter_complex '[0:v] trim=end=" +
    getTrueValueOfSeconds3(Timetosec3(this->StartTime)) + " [a],[0:a] atrim=end=" +
    getTrueValueOfSeconds3(Timetosec3(this->StartTime)) + " [b],[0:v] trim=start=" +
    getTrueValueOfSeconds3(Timetosec3(this->EndTime)) + " ,setpts=PTS-STARTPTS [c],[0:a] atrim=start=" +
    getTrueValueOfSeconds3(Timetosec3(this->EndTime)) + " ,asetpts=PTS-STARTPTS [d]," +
    " [a][b][c][d] concat=n=2:v=1:a=1 [v][a]' -map '[v] -map'[a]' \"" + this->outputVideo.getVideoFullName() + "\"";
  this->setCommand(str);
}

void Trim::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Trim::getEditCommand() {
  return this->command.getCommand();
}

Trim::~Trim()
{
}

