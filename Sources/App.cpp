#include "../Headers/App.hpp"
using namespace std;
#include <QDebug>
App::App()
{

}
#include <QString>
App::~App() {

}

void App::render() {                             //This will call All Edits "Do" Function
  vector <string> commands;
  std::string ss = "";                           //                      Terminal Command
  if (this->Edits.size() == 0) {
    Error* msg = new Error("Rendering FAILD", "Please Do something that I want to render it!");
    delete msg;
  } 
  else if
  (this->Edits.size() == 1) {
      this->Edits[0]->Do(this->InputVideo.getVideoFullName(),//                  Getting Edit Command
                                    this->OutputVideo.getVideoFullName());
      commands.push_back(this->Edits[0]->getEditCommand());  //        Pushing edit Command to vector
      qDebug() << QString::fromStdString(this->Edits[0]->getEditCommand());
      ss += this->Edits[0]->getEditCommand();          //     Adding Semicolon between Commands
      system(ss.c_str());                            //                      Calling Commands
  }
  else 
  {
    for (size_t i = 0; i < this->Edits.size(); i++)
    {
      if (i == 0) {
        this->Edits[i]->Do(this->InputVideo.getVideoFullName(),//                  Getting Edit Command
                                      "thisfileisfortest_makeuniqe_J3DyZofE6cEf7ywenum=" + to_string((i + 1)) + ".mp4");
        commands.push_back(this->Edits[i]->getEditCommand());  //        Pushing edit Command to vector
        ss += this->Edits[i]->getEditCommand() + ";";          //     Adding Semicolon between Commands
      }



      else if (i == this->Edits.size()-1) {
        this->Edits[i]->Do("thisfileisfortest_makeuniqe_J3DyZofE6cEf7ywenu=" + to_string(i) + ".mp4",//                  Getting Edit Command
                                      this->OutputVideo.getVideoFullName());
        commands.push_back(this->Edits[i]->getEditCommand());  //        Pushithis->Edits[i] Command to vector
        ss += this->Edits[i]->getEditCommand() + ";";          //     Adding Semicolon between Commands
      }


      
      else {
        this->Edits[i]->Do("thisfileisfortest_makeuniqe_J3DyZofE6cEf7ywenu=" + to_string(i) + ".mp4",//                  Getting Edit Command
                                      "thisfileisfortest_makeuniqe_J3DyZofE6cEf7ywenum=" + to_string(i + 1) + ".mp4");
        commands.push_back(this->Edits[i]->getEditCommand());  //        Pushing edit Command to vector
        ss += this->Edits[i]->getEditCommand() + ";";          //     Adding Semicolon between Commands
        ss = ss.substr(0, ss.size()-1);                //                   removing last " ; "
      }


      system(ss.c_str());                            //                      Calling Commands
  }
    }

}

void App::addEdit(Edit* t) {
  this->Edits.push_back(t);
}

void App::setInVideoFullName (string n) {
  this->InputVideo.setVideoFullName(n);
}

void App::setOutVideoFullName (string n) {
  this->OutputVideo.setVideoFullName(n);
}

void App::setInVideoName (string n) {
  this->InputVideo.setVideoName(n);
}

void App::setOutVideoName (string n) {
  this->OutputVideo.setVideoName(n);
}

void App::setInVideoFormat(std::string n) {
  this->InputVideo.setVideoFormat(n);
}

void App::setOutVideoFormat(std::string n) {  
  this->OutputVideo.setVideoFormat(n);
}

Video App::getOutVid() {
  return this->OutputVideo;
}

Video App::getInVid() {
  return this->InputVideo;
}
