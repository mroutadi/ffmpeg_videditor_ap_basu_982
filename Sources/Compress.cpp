#include "../Headers/Compress.hpp"
using namespace std;

Compress::Compress():DecreaseLevel(0) {

}

Compress::~Compress() {

}

void Compress::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Compress::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

void Compress::setDecreaseLevel(unsigned int n) {
  if (n > 50)
  {
    n = 50;
  }
  this->DecreaseLevel = n;
}

void Compress::Do (Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-i \"" + this->inputVideo.getVideoFullName() +
  "\" -vcodec libx265 -crf " + to_string(this->DecreaseLevel) +
  " \"" + this->outputVideo.getVideoFullName() + "\"";
  this->setCommand(str);
}

void Compress::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Compress::getEditCommand() {
  return this->command.getCommand();
}
