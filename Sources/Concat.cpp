#include "../Headers/Concat.hpp"
using namespace std;

Concat::Concat() {

}

Concat::~Concat() {

}

void Concat::setInputVideo(std::vector<Video> Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Concat::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

void Concat::Do (Video I, Video O) {
  this->setOutputVideo(O);
  string str = "-i \"concat:" + this->inputVideo[0].getVideoFullName();
  for (size_t i = 1; i < this->inputVideo.size(); i++)
  {
    str += "|" + this->inputVideo[i].getVideoFullName();
  }
  str += "\" -c copy " + O.getVideoFullName();
  this->setCommand(str);
}

void Concat::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Concat::getEditCommand() {
  return this->command.getCommand();
}
