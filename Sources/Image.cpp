#include "../Headers/Image.hpp"
using namespace std;

Image::Image () {

}

Image::~Image () {

}

void Image::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Image::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

float TimetosecondConvertor(Time t) {
  return (t.getHour()*3600 + t.getMin()*60 + t.getSec() + t.getMSec());
}

string getTrueValueOfSeconds(float f) {
  string temp = to_string(f);
  
  if ((int(f*1000) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-3));    
  }
  else if ((int(f*100) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-4));    
  }
  else if ((int(f*10) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-5));    
  }
  else
  {
    return (temp.substr(0, temp.length()-7));    
  }
}

void Image::Do(Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-i \"" + this->inputVideo.getVideoFullName() + "\" -i \"" +
  this->ImageFileName + "\" -filter_complex \"" +
  "[1:v] scale=w=" + to_string(this->width) +":h=" + to_string(this->height) +
  " [t],[0:v][t] overlay=" +
  to_string(this->topDist) + ":" + to_string(this->leftDist) + 
  ":enable='between(t," + getTrueValueOfSeconds(this->StartTime) + "," + 
  getTrueValueOfSeconds(this->EndTime) + ")'\" \"" + this->outputVideo.getVideoFullName() + "\"";
  this->setCommand(str);
}

void Image::setImageName(string I) {
  this->ImageFileName = I;
}

void Image::settopDist(int D) {
  this->topDist = D;
}

void Image::setleftDist(int D) {
  this->leftDist = D;
}

void Image::setHeight (float h) {
  this->height = h;
}

void Image::setWidth (float w) {
  this->width = w;
}

void Image::setStartTime(Time t) {
  this->StartTime = TimetosecondConvertor(t);
}

void Image::setEndTime(Time t) {
  this->EndTime = TimetosecondConvertor(t);
}

void Image::setCommand(string c) {
  this->command.setCommand(c);
}

string Image::getEditCommand() {
  return this->command.getCommand();
}
