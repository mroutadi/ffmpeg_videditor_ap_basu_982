#include "../Headers/Blur.hpp"
using namespace std;

Blur::Blur () {

}

Blur::~Blur () {

}

void Blur::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Blur::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

string getTrueValueOfSeconds2(float f) {  //this will create true number format HH:MM:SS.mmm
  string temp = to_string(f);
  
  if ((int(f*1000) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-3));    
  }
  else if ((int(f*100) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-4));    
  }
  else if ((int(f*10) % 10) != 0)
  {
    return (temp.substr(0, temp.length()-5));    
  }
  else
  {
    return (temp.substr(0, temp.length()-7));    
  }
}

float Timetosec2(Time t) {                        //Time(class) to seconds converter
  return (t.getHour()*3600 + t.getMin()*60 + t.getSec() + t.getMSec());
}

void Blur::Do(Video i, Video o) {                 //this will create Bluring command
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-i \"" + this->inputVideo.getVideoFullName() +
  "\" -filter_complex \"[0:v]crop=" + to_string(this->width) + ":" +
  to_string(this->height) + ":" + to_string(this->topDist) + ":" + 
  to_string(this->leftDist) + ",boxblur=" + to_string(this->BlurPercent) + "[fg];[0:v][fg]overlay=" +
  to_string(this->topDist) + ":" + to_string(this->leftDist) + 
  ":enable='between(t," + getTrueValueOfSeconds2(this->StartTime) + "," + 
  getTrueValueOfSeconds2(this->EndTime) + ")'\" \"" + this->outputVideo.getVideoFullName() + "\"";
  this->setCommand(str);
}

void Blur::setBlurPercent(unsigned int I) {
  this->BlurPercent = I;
}

void Blur::settopDist(int D) {
  this->topDist = D;
}

void Blur::setleftDist(int D) {
  this->leftDist = D;
}

void Blur::setHeight(unsigned int D) {
  this->height = D;
}

void Blur::setWidth(unsigned int D) {
  this->width = D;
}

void Blur::setStartTime(Time t) {
  this->StartTime = Timetosec2(t);
}

void Blur::setEndTime(Time t) {
  this->EndTime = Timetosec2(t);
}

void Blur::setCommand(string c) {
  this->command.setCommand(c);
}

string Blur::getEditCommand() {
  return this->command.getCommand();
}
