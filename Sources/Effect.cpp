#include "../Headers/Effect.hpp"

Effect::Effect():EffectName("")
{
}

Effect::~Effect()
{
}

void Effect::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Effect::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

void Effect::Do (Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  std::string str = "-i " + this->inputVideo.getVideoFullName() +
  " -vf ";
  if (this->EffectName == "BAW")
  {
    str += "hue=s=0";
  } else 
  if (this->EffectName == "Brighter")
  {
    str += "eq=brightness=0.25";
  } else 
  if (this->EffectName == "Darker")
  {
    str += "eq=brightness=-0.25";
  } else 
  if (this->EffectName == "Green")
  {
    str += "eq=gamma_r=0.2";
  } else 
  if (this->EffectName == "LContrast")
  {
    str += "eq=contrast=0.5";
  } else 
  if (this->EffectName == "MContrast")
  {
    str += "eq=contrast=1.25";
  } else 
  if (this->EffectName == "Red")
  {
    str += "eq=gamma_r=1.8";
  } else 
  if (this->EffectName == "Sepia")
  {
    str += "\"colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131\"";
  }
  str += " " + this->outputVideo.getVideoFullName();
  this->setCommand(str);
}

void Effect::setEffect(std::string e) {
  this->EffectName = e;
}

void Effect::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Effect::getEditCommand() {
  return this->command.getCommand();
}
