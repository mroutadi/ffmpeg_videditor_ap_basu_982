#include "../Headers/Resolution.hpp"
using namespace std;

Resolution::Resolution() {

}

Resolution::~Resolution() {

}

void Resolution::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Resolution::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

void Resolution::Do(Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-i \"" + this->inputVideo.getVideoFullName()
  + "\" -c:a copy -s " + this->resolution + " \"" +
  this->outputVideo.getVideoFullName() + "\"";
  this->setCommand(str);
}

void Resolution::setResolution(std::string ResolutionsENumMember) {
  this->resolution = ResolutionsENumMember;
}

void Resolution::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Resolution::getEditCommand() {
  return this->command.getCommand();
}
