#include "../Headers/Convert.hpp"
using namespace std;

Convert::Convert() {

}

Convert::~Convert() {

}

void Convert::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Convert::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

void Convert::Do(Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-i \"" + this->inputVideo.getVideoFullName()
  + "\" \"" + this->outputVideo.getVideoFullName() + "\"";
  this->setCommand(str);
}

void Convert::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Convert::getEditCommand() {
  return this->command.getCommand();
}

