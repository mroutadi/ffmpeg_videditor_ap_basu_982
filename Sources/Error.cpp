#include "../Headers/Error.hpp"

Error::Error(std::string T, std::string M)
{
    QMessageBox msg;
    msg.setWindowTitle(QString::fromStdString(T));
    msg.setText(QString::fromStdString(M));
    msg.exec();
}

Error::~Error()
{

}

