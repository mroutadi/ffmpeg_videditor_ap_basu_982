#include "../Headers/Cut.hpp"
using namespace std;

Cut::Cut() {
  
}

void Cut::setStartTime(Time S) {
  this->StartTime = S;
}

void Cut::setEndTime(Time E) {
  this->EndTime = E;
}

void Cut::setInputVideo(Video Inputvideo) {
  this->inputVideo = Inputvideo;  
}

void Cut::setOutputVideo(Video Outputvideo) {
  this->outputVideo = Outputvideo;
}

void Cut::Do (Video i, Video o) {
  this->setInputVideo(i);
  this->setOutputVideo(o);
  string str = "-ss " + this->StartTime.getTime() + " -i \"" +
  this->inputVideo.getVideoFullName() + "\" -to " +
  this->EndTime.getTime() + " -c copy \"" +
  this->outputVideo.getVideoFullName() + "\"";
  this->setCommand(str);
}

void Cut::setCommand(std::string c) {
  this->command.setCommand(c);
}

std::string Cut::getEditCommand() {
  return this->command.getCommand();
}

Cut::~Cut()
{
}

