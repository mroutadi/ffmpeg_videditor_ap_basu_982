#include "videoeditor.h"
#include "./ui_videoeditor.h"

using namespace std;

VideoEditor::VideoEditor(QWidget *parent) : QMainWindow(parent), ui(new Ui::VideoEditor)
{
    ui->setupUi(this);                     //Set up a user interface
    this->Setpages();                    //Set First videoeditor views and components show
    this->myApp = new App();        //Creating an app for handling video edits
}

void VideoEditor::Setpages()
{
    ui->FileWidget->setVisible(1);
    ui->EditWidget->setVisible(0);
    ui->EffectWidget->setVisible(0);
    ui->HelpWidget->setVisible(0);

    ui->EditButton->setEnabled(false);      //These are related to video so without video are inaccessable
    ui->EffectButton->setEnabled(false);

    ui->BlurInfo->setVisible(1);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}

VideoEditor::~VideoEditor() //Destructor
{
    delete ui;
    delete myApp;
}
/*              UI button handlers              */
void VideoEditor::on_FileButton_clicked()
{
    ui->FileWidget->setVisible(1);
    ui->EditWidget->setVisible(0);
    ui->EffectWidget->setVisible(0);
    ui->HelpWidget->setVisible(0);
}
void VideoEditor::on_EditButton_clicked()
{
    ui->FileWidget->setVisible(0);
    ui->EditWidget->setVisible(1);
    ui->EffectWidget->setVisible(0);
    ui->HelpWidget->setVisible(0);
}
void VideoEditor::on_EffectButton_clicked()
{
    ui->FileWidget->setVisible(0);
    ui->EditWidget->setVisible(0);
    ui->EffectWidget->setVisible(1);
    ui->HelpWidget->setVisible(0);
}
void VideoEditor::on_HelpButton_clicked()
{
    ui->FileWidget->setVisible(0);
    ui->EditWidget->setVisible(0);
    ui->EffectWidget->setVisible(0);
    ui->HelpWidget->setVisible(1);
}
void VideoEditor::on_Blur_clicked()
{
    ui->BlurInfo->setVisible(1);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Cut_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(1);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Trim_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(1);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Subtitle_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(1);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Resolution_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(1);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Concat_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(1);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Image_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(1);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Convert_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(1);
    ui->CompressInfo->setVisible(0);
}
void VideoEditor::on_Compress_clicked()
{
    ui->BlurInfo->setVisible(0);
    ui->CutInfo->setVisible(0);
    ui->TrimInfo->setVisible(0);
    ui->SubtitleInfo->setVisible(0);
    ui->ResolutionInfo->setVisible(0);
    ui->ConcatInfo->setVisible(0);
    ui->ImageInfo->setVisible(0);
    ui->ConvertInfo->setVisible(0);
    ui->CompressInfo->setVisible(1);
}

/*              Editor Controllers             */
void VideoEditor::on_ImageFolderButton_clicked()
{   //Getting Input Video Name from user
    auto ImageFilename = QFileDialog::getOpenFileName(this,
    tr("Please Select Your Image ..."), "/home/mroutadi/Downloads", tr("Image Files (*.png *.jpg *.jpeg)"));
    ui->ImageAddress->setText(ImageFilename);
    Error* msg = new Error("Image", "Your Image added Successfully!");       //Message for successfull
    delete msg;
}
void VideoEditor::on_ImageSubmit_clicked()
{
    auto image_ptr = new Image();
    //I forgot to check End before Start so there is no error :(    please use true :)
    Time start(ui->ImageStartTimeH->value(), ui->ImageStartTimeM->value(), ui->ImageStartTimeS->value(), ui->ImageStartTimeMS->value());
    Time end(ui->ImageEndTimeH->value(), ui->ImageEndTimeM->value(), ui->ImageEndTimeS->value(), ui->ImageEndTimeMS->value());
    image_ptr->setStartTime(start);
    image_ptr->setEndTime(end);
    try
    {
        std::string address = ui->ImageAddress->text().toUtf8().constData();
        if (address.size() == 0)
        {
            throw std::invalid_argument("There is no image to add!");
        } else {
            image_ptr->setImageName(ui->ImageAddress->text().toUtf8().constData());
        }
        
        image_ptr->settopDist(ui->ImageTopDistance->value());
        image_ptr->setleftDist(ui->ImageLeftDistance->value());
        if (ui->ImageHeight->value()==0) {
            throw std::invalid_argument("Your Height must be greather than 0!");
        } else {
            image_ptr->setHeight(ui->ImageHeight->value());
        }
        if (ui->ImageWidth->value()==0) {
            throw std::invalid_argument("Your Width must be greather than 0!");
        } else {
            image_ptr->setWidth(ui->ImageWidth->value());
        }
        this->myApp->addEdit(image_ptr);
        Error* msg = new Error("Image on Video", "Adding Image on your video added to Edit List Successfully!");
        delete msg;
    }
    catch(const std::exception& e)
    {
        delete image_ptr;
        Error* msg = new Error("Add image FAILD", e.what());
        delete msg;
    }
}
void VideoEditor::on_NewProjectButton_clicked()
{
    auto InputVideoName = QFileDialog::getOpenFileName(this,
        tr("Please Select Your Video ..."),
        "/home/mroutadi/Downloads",
        tr("Video (*.mp4 *.mov *.mkv *.mpg4 *.gif)"));
    try {
        this->myApp->setInVideoFullName(InputVideoName.toUtf8().constData());
        this->myApp->setOutVideoFullName(InputVideoName.toUtf8().constData());
        if (InputVideoName != "")
        {
            ui->FileButton->setEnabled(false);
            ui->EditButton->setEnabled(true);
            ui->EffectButton->setEnabled(true);
            ui->EditButton->click();
        } else {
            throw std::invalid_argument("Please select a video for edit");
        }
    }
    catch(const std::exception& e)
    {
        Error* msg = new Error("Empty Video ...", e.what());
        delete msg;
    }
}
void VideoEditor::on_BlurPercent_valueChanged(int position)
{
    ui->BlurPercentNum->setText(QString::number(position));
}
void VideoEditor::on_BlurSubmit_clicked()
{
    auto blur_ptr = new Blur();
    Time start(ui->BlurStartTimeH->value(), ui->BlurStartTimeM->value(), ui->BlurStartTimeS->value(), ui->BlurStartTimeMS->value());
    Time end(ui->BlurEndTimeH->value(), ui->BlurEndTimeM->value(), ui->BlurEndTimeS->value(), ui->BlurEndTimeMS->value());
    blur_ptr->setStartTime(start);
    blur_ptr->setEndTime(end);
    try
    {
        blur_ptr->setBlurPercent(ui->BlurPercent->value());
        if (ui->BlurHeight->value()==0) {
            throw std::invalid_argument("Your Height must be greather than 0!");
        } else {
            blur_ptr->setHeight(ui->BlurHeight->value());
        }
        if (ui->BlurWidth->value()==0) {
            throw std::invalid_argument("Your Width must be greather than 0!");
        } else {
            blur_ptr->setWidth(ui->BlurWidth->value());
        }
        blur_ptr->settopDist(ui->BlurTopDistance->value());
        blur_ptr->setleftDist(ui->BlurLeftDistance->value());
        this->myApp->addEdit(blur_ptr);
        Error* msg = new Error("Blur on Video", "Adding Blur on your video added to Edit List Successfully!");
    }
    catch(const std::exception& e)
    {
        delete blur_ptr;
        Error* msg = new Error("Add Blur FAILD", e.what());
        delete msg;
    }
    
}
void VideoEditor::on_VideoCompressLevel_valueChanged(int value)
{
    ui->CompressPercentNum->setText(QString::number(value));
}
void VideoEditor::on_CompressSubmit_clicked()
{
    auto compress_ptr = new Compress();
    compress_ptr->setDecreaseLevel(ui->VideoCompressLevel->value());
    this->myApp->addEdit(compress_ptr);
    Error* msg = new Error("Compressing Video", "Compressing Video added to Edit List Successfully!");
}
void VideoEditor::on_ConvertSubmit_clicked()
{
    QButtonGroup group;
    QList<QRadioButton *> allButtons = ui->formatgroupBox->findChildren<QRadioButton *>();
    //        qDebug() << allButtons.size();
    for (int i = 0; i < allButtons.size(); ++i)
    {
        group.addButton(allButtons[i], i);
    }
    //                qDebug() << group.checkedId();
    //                qDebug() << group.checkedButton();
    switch (group.checkedId())
    {
    case 0:
        this->myApp->setOutVideoFormat("mp3");
        break;
    case 1:
        this->myApp->setOutVideoFormat("ogg");
        break;
    case 2:
        this->myApp->setOutVideoFormat("gif");
        break;
    case 3:
        this->myApp->setOutVideoFormat("mp4");
        break;
    case 4:
        this->myApp->setOutVideoFormat("mkv");
        break;
    case 5:
        this->myApp->setOutVideoFormat("mov");
        break;
    case 6:
        this->myApp->setOutVideoFormat("av1");
        break;
    case 7:
        this->myApp->setOutVideoFormat("mpeg");
        break;
    default:
        this->myApp->setOutVideoFormat(this->myApp->getInVid().getVideoFullName());
        break;
    }
    auto convert_ptr = new Convert();
    this->myApp->addEdit(convert_ptr);
    Error* msg = new Error("Converting Video", "Your Video Format Converting added to Edit List Successfully!");
}
void VideoEditor::on_SubtitleFolderButton_clicked()
{
    auto SubtitleName = QFileDialog::getOpenFileName(this,
                                                     tr("Please Select Subtitle File ..."), "/home/mroutadi/Downloads", tr("Subtitle (*.srt)"));
    if (SubtitleName != "")
    {
        this->ui->SubtitleAddress->setText(SubtitleName);
    }
    //    else Error
}
void VideoEditor::on_SubtitleSubmit_clicked()
{
    Subtitle *subtitle_ptr = new Subtitle();
    try
    {
        std::string address = this->ui->SubtitleAddress->text().toUtf8().constData();
        if (!address.size())
        {
            throw std::invalid_argument("There is no subtitle to add!");
        } else {
            subtitle_ptr->setSubtitle(this->ui->SubtitleAddress->text().toUtf8().constData());
        }
        subtitle_ptr->setSubtitleDelay(this->ui->SubtitleDelay->value());
        this->myApp->addEdit(subtitle_ptr);
        Error* msg = new Error("Adding Subtitle", "Adding Subtitle to your video added to Edit List Successfully!");
        delete msg;
    }
    catch(const std::exception& e)
    {
        delete subtitle_ptr;
        Error* msg = new Error("Add subtitle FAILD", e.what());
        delete msg;
    }
    
}
void VideoEditor::on_TrimSubmit_clicked()
{
    Trim *trim_ptr = new Trim();
    Time start(ui->TrimStartTimeH->value(), ui->TrimStartTimeM->value(), ui->TrimStartTimeS->value(), ui->TrimStartTimeMS->value());
    Time end(ui->TrimEndTimeH->value(), ui->TrimEndTimeM->value(), ui->TrimEndTimeS->value(), ui->TrimEndTimeMS->value());
    trim_ptr->setStartTime(start);
    trim_ptr->setEndTime(end);
    this->myApp->addEdit(trim_ptr);
    Error* msg = new Error("Triming Video", "Triming your video added to Edit List Successfully!");
    delete msg;
}
void VideoEditor::on_ConcatFolderButton_clicked()
{
    auto VideoName = QFileDialog::getOpenFileName(this,
        tr("Please Select Your Video ..."),
        "/home/mroutadi/Downloads",
        tr("Video (*.mp4 *.mov *.mkv *.mpg4 *.gif)"));
    if (VideoName.size())
    {
        VidForCon.push_back(VideoName);
        this->ui->ConcatVidNums->setValue(VidForCon.size());
        this->ui->ConcatMainIndex->setMaximum(VidForCon.size() + 1);
    }else {
        Error* msg = new Error("Adding Video for concat FAILD", "Please Select a video to add concat list!");
        delete msg;
    }
}
void VideoEditor::on_ConcatSubmit_clicked()
{
    auto concat_ptr = new Concat();
    vector<Video> videos;
    try
    {
        if (!this->VidForCon.size())
        {
            throw std::invalid_argument("Please add at least one video for concating!");
        } else {
            for (size_t i = 0; i < this->VidForCon.size(); i++)
            {
                if (i == this->ui->ConcatMainIndex->value() - 1)
                {
                    videos.push_back(this->myApp->getInVid());
                    Video temp;
                    temp.setVideoFullName(this->VidForCon[i].toUtf8().constData());
                    videos.push_back(temp);
                }
                else
                {
                    Video temp;
                    temp.setVideoFullName(this->VidForCon[i].toUtf8().constData());
                    videos.push_back(temp);
                }
            }
            if (this->ui->ConcatMainIndex->value() > this->VidForCon.size())
            {
                videos.push_back(this->myApp->getInVid());
            }
            concat_ptr->setInputVideo(videos);
            this->myApp->addEdit(concat_ptr);
            Error* msg = new Error("Concating Videos", "Concating Videos added to Edit List Successfully!");
            delete msg;
        }
    }
    catch(const std::exception& e)
    {
        delete concat_ptr;
        Error* msg = new Error("Concating Videos FAILD", e.what());
        delete msg;
    }
    
}
void VideoEditor::on_CutSubmit_clicked()
{
    auto cut_ptr = new Cut();
    Time start(ui->CutStartTimeH->value(), ui->CutStartTimeM->value(), ui->CutStartTimeS->value(), ui->CutStartTimeMS->value());
    Time end(ui->CutEndTimeH->value(), ui->CutEndTimeM->value(), ui->CutEndTimeS->value(), ui->CutEndTimeMS->value());
    cut_ptr->setStartTime(start);
    cut_ptr->setEndTime(end);
    this->myApp->addEdit(cut_ptr);
    Error* msg = new Error("Cutting Video", "Cutting your video added to Edit List Successfully!");
    delete msg;
}
void VideoEditor::on_ResolutionSubmit_clicked()
{
    QButtonGroup group;
    QList<QRadioButton *> allButtons = ui->ResolutiongroupBox->findChildren<QRadioButton *>();
    //        qDebug() << allButtons.size();
    for (int i = 0; i < allButtons.size(); ++i)
    {
        group.addButton(allButtons[i], i);
    }
    map <string, string> Resolutions;
    Resolutions.insert({"480p", "640×480"});
    Resolutions.insert({"576p", "1024×576"});
    Resolutions.insert({"648p", "1152×648"});
    Resolutions.insert({"720p", "1280x720"});
    Resolutions.insert({"1080p","1920x1080"});
    Resolutions.insert({"1440p","2560x1440"});
    Resolutions.insert({"4k","3840x2160"});
    Resolutions.insert({"8k","7680x4320"});

    auto resolution_ptr = new Resolution();

    switch (group.checkedId())
    {
    case 0:
        resolution_ptr->setResolution(Resolutions["480p"]);
        break;
    case 1:
        resolution_ptr->setResolution(Resolutions["576p"]);
        break;
    case 2:
        resolution_ptr->setResolution(Resolutions["648p"]);
        break;
    case 3:
        resolution_ptr->setResolution(Resolutions["720p"]);
        break;
    case 4:
        resolution_ptr->setResolution(Resolutions["1080p"]);
        break;
    case 5:
        resolution_ptr->setResolution(Resolutions["1440p"]);
        break;
    case 6:
        resolution_ptr->setResolution(Resolutions["4k"]);
        break;
    case 7:
        resolution_ptr->setResolution(Resolutions["8k"]);
        break;
    }
    this->myApp->addEdit(resolution_ptr);
    Error* msg = new Error("Video Resolution", "Changing your video resolution added to Edit List Successfully!");
    delete msg;
}
void VideoEditor::on_renderButton_clicked()
{
    auto Saving = QFileDialog::getSaveFileName(this,
    tr("Set Output Address and Name with out extention "));
    this->myApp->setOutVideoName(Saving.toUtf8().constData());
    this->myApp->setOutVideoFormat(this->myApp->getInVid().getVideoFormat());
    Error* msg = new Error("Rendering", "Now Your Video is rendering, go to your files!");
    delete msg;
    this->myApp->render();
}
void VideoEditor::on_EffectSubmit_clicked()
{
    QButtonGroup group;
    QList<QRadioButton *> allButtons = ui->effectgroupBox->findChildren<QRadioButton *>();
    for (int i = 0; i < allButtons.size(); ++i)
    {
        group.addButton(allButtons[i], i);
    }

    if (group.checkedId()){
        auto effect_ptr = new Effect();
        switch (group.checkedId())
        {
        case 8:
            effect_ptr->setEffect("BAW");
            break;
        case 4:
            effect_ptr->setEffect("Brighter");
            break;
        case 7:
            effect_ptr->setEffect("Darker");
            break;
        case 2:
            effect_ptr->setEffect("Green");
            break;
        case 3:
            effect_ptr->setEffect("LContrast");
            break;
        case 1:
            effect_ptr->setEffect("MContrast");
            break;
        case 5:
            effect_ptr->setEffect("Red");
            break;
        case 6:
            effect_ptr->setEffect("Sepia");
            break;
        }
        this->myApp->addEdit(effect_ptr);
        Error* msg = new Error("Video Effect", "Video Effect added to Edit List Successfully!");
        delete msg;
    }
}

