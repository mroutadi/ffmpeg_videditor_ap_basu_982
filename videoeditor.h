#ifndef VIDEOEDITOR_H
#define VIDEOEDITOR_H

#include <QMainWindow>
#include <QLayout>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QPixmap>
#include <QBitmap>
#include <QLabel>
#include <QLineEdit>
#include <QFileDialog>
#include <QString>
#include <QButtonGroup>
#include <QDebug>
#include <QMessageBox>

#include <exception>
#include <stdexcept>

#include <Headers/App.hpp>
#include <Headers/Time.hpp>
#include <Headers/Effect.hpp>

QT_BEGIN_NAMESPACE
namespace Ui {
    class VideoEditor;
}
QT_END_NAMESPACE

class VideoEditor : public QMainWindow
{
    Q_OBJECT

public:
    VideoEditor(QWidget *parent = nullptr);
    ~VideoEditor();
    void Setpages();

private slots:
    void on_FileButton_clicked();
    void on_HelpButton_clicked();
    void on_EditButton_clicked();
    void on_EffectButton_clicked();
    void on_Blur_clicked();
    void on_Cut_clicked();
    void on_Trim_clicked();
    void on_Subtitle_clicked();
    void on_Resolution_clicked();
    void on_Concat_clicked();
    void on_Image_clicked();
    void on_Convert_clicked();
    void on_Compress_clicked();
    void on_ImageFolderButton_clicked();
    void on_ImageSubmit_clicked();
    void on_NewProjectButton_clicked();
    void on_BlurPercent_valueChanged(int position);
    void on_BlurSubmit_clicked();
    void on_VideoCompressLevel_valueChanged(int value);
    void on_CompressSubmit_clicked();
    void on_ConvertSubmit_clicked();
    void on_SubtitleFolderButton_clicked();
    void on_SubtitleSubmit_clicked();
    void on_TrimSubmit_clicked();
    void on_ConcatFolderButton_clicked();
    void on_ConcatSubmit_clicked();
    void on_CutSubmit_clicked();
    void on_ResolutionSubmit_clicked();
    void on_renderButton_clicked();
    void on_EffectSubmit_clicked();

private:
    Ui::VideoEditor *ui;
    App* myApp;
    std::vector <QString> VidForCon;
};
#endif // VIDEOEDITOR_H
