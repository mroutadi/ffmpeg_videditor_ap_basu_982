#include "videoeditor.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    VideoEditor V;
    V.show();
    return a.exec();
}
