#ifndef IMAGE_HPP
# define IMAGE_HPP


#include <iostream>
#include <string>

#include "Edit.hpp"

class Image : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;

  std::string ImageFileName;
  int topDist;
  int leftDist;
  float width;
  float height;
  float StartTime;
  float EndTime;
  
public:
  Image();
  ~Image();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setImageName(std::string Image_Name);
  void settopDist(int Distance_your_pic_from_top_side);
  void setleftDist(int Distance_your_pic_from_left_side);
  void setWidth(float width);
  void setHeight(float height);
  void setStartTime(Time time_when_your_image_starting_to_show);
  void setEndTime(Time time_when_your_image_finish_showing);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;

};

#endif