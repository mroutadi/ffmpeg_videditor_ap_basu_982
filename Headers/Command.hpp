#ifndef COMMAND_HPP
# define COMMAND_HPP

#include <iostream>
#include <string>
#include <vector>

class Command
{
private:
  std::string CommandLine;
public:
  Command();
  ~Command();
  void setCommand(std::string Command);
  std::string getCommand();
};

#endif