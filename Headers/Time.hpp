#ifndef TIME_HPP
# define TIME_HPP

#include <iostream>
#include <string>

class Time {
private:
  unsigned int Hour;
  unsigned int Min;
  unsigned int Sec;
  float MSec;
public:
  Time();
  Time(unsigned int Hour);
  Time(unsigned int Hour, unsigned int Min);
  Time(unsigned int Hour, unsigned int Min, unsigned int Sec);
  Time(unsigned int Hour, unsigned int Min, unsigned int Sec, float MSec);
  ~Time();
  std::string getTime();
  int getHour();
  int getMin();
  int getSec();
  float getMSec();
};

#endif