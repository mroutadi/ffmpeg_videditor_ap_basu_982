#ifndef CONCAT_HPP
# define CONCAT_HPP

#include <iostream>
#include <string>

#include "Edit.hpp"

class Concat : public Edit
{
private:
  Command command;
  std::vector<Video> inputVideo;
  Video outputVideo;
  
public:
  Concat();
  ~Concat();

  void setInputVideo(std::vector<Video> Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;

};

#endif