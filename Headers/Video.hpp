#ifndef VIDEO_HPP
# define VIDEO_HPP

#include <iostream>
#include <string>
#include <cstddef>


class Video {
private:
  std::string VideoName;
  std::string VideoFormat;
public:
  Video();
  Video(std::string VideoName);
  Video(std::string VideoName, std::string OutputVideoName);
  virtual ~Video();
  void setVideoFullName(std::string VideoFullName);
  void setVideoName(std::string VideoName);
  void setVideoFormat(std::string VideoFormat);
  std::string getVideoFullName();
  std::string getVideoName();
  std::string getVideoFormat();
};

#endif