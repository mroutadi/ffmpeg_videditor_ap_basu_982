#ifndef ERROR_HPP
# define ERROR_HPP

#include <iostream>
#include <string>
#include <QString>
#include <QMessageBox>

class Error {
public:
  Error(std::string Title, std::string Message);
  ~Error();
};

#endif
