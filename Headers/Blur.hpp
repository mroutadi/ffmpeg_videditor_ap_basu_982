#ifndef BLUR_HPP
# define BLUR_HPP

#include <iostream>
#include <string>

#include "Edit.hpp"

class Blur : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;

  unsigned int BlurPercent;
  unsigned int height;
  unsigned int width;
  int topDist;
  int leftDist;
  float StartTime;
  float EndTime;
  
public:
  Blur();
  ~Blur();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setBlurPercent(unsigned int Blur_Percent);
  void setHeight(unsigned int height);
  void setWidth(unsigned int width);
  void settopDist(int Distance_your_blur_from_top_side);
  void setleftDist(int Distance_your_blur_from_left_side);
  void setStartTime(Time time_when_your_blur_starting_to_show);
  void setEndTime(Time time_when_your_blur_finish_showing);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;

};

#endif