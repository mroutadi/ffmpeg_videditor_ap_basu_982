#ifndef COMPRESS_HPP
# define COMPRESS_HPP

#include <iostream>
#include <string>

#include "Edit.hpp"

class Compress : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;

  unsigned int DecreaseLevel;
  
public:
  Compress();
  ~Compress();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setDecreaseLevel(unsigned int Level);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;

};

#endif