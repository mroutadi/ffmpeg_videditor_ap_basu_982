#ifndef EFFECT_HPP
#define EFFECT_HPP

#include "string"

#include "Edit.hpp"

class Effect: public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;

  std::string EffectName;
  
public:
  Effect();
  ~Effect();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setEffect(std::string EffectName);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;
};

#endif  //!__EFFECT__H__