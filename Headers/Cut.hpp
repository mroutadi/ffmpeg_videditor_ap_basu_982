#ifndef CUT_HPP
# define CUT_HPP

#include <iostream>
#include <string>

#include "Edit.hpp"

class Cut : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;

  Time StartTime;
  Time EndTime;

public:
  Cut();
  ~Cut();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setStartTime(Time Start);
  void setEndTime(Time End);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;

};

#endif