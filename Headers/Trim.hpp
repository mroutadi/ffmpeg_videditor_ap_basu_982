#ifndef TRIM_HPP
# define TRIM_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Edit.hpp"

class Trim : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;


  Time StartTime;
  Time EndTime;

public:
  Trim();
  ~Trim();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setStartTime(Time Start);
  void setEndTime(Time End);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;

};

#endif