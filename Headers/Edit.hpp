#ifndef EDIT_HPP
# define EDIT_HPP

#include <vector>
#include <string>
#include <iomanip>

#include "Video.hpp"
#include "Time.hpp"
#include "Command.hpp"

class Edit
{
private:
  Command command;
public:
  Edit();
  virtual ~Edit();
  virtual void Do(Video Input, Video Output) = 0;
  virtual void setCommand(std::string command) = 0;
  virtual std::string getEditCommand() = 0;
};
/*
  1.Do function that is shared between all Edits is for creating command that will do youe task
  2.SetCommand will set that to the class command member
  3.getEditCommand will return that
*/

#endif
