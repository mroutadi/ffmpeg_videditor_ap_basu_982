#ifndef SUBTITLE_HPP
# define SUBTITLE_HPP


#include <iostream>
#include <string>
#include <fstream>

#include "Edit.hpp"

class Subtitle : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;

  std::string SubtitleName;
  double SubtitleDelay;
  
public:
  Subtitle();
  ~Subtitle();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setSubtitle(std::string Subtitle_File_Name);
  void setSubtitleDelay(double Double_Number_For_Subtitle_Delay);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;

};

#endif