#ifndef APP_HPP
# define APP_HPP

#include <vector>
#include <string>
#include <map>

#include "Video.hpp"
#include "Edit.hpp"
#include "Concat.hpp"
#include "Compress.hpp"
#include "Convert.hpp"
#include "Cut.hpp"
#include "Resolution.hpp"
#include "Subtitle.hpp"
#include "Time.hpp"
#include "Image.hpp"
#include "Blur.hpp"
#include "Trim.hpp"
#include "Effect.hpp"
#include "Error.hpp"

class App {
private:
  std::vector <Edit*> Edits;
  Video InputVideo;
  Video OutputVideo;
  
public:
  App();
  ~App();
  void setInVideoFullName(std::string);
  void setOutVideoFullName(std::string);
  void setInVideoName(std::string);
  void setOutVideoName(std::string);
  void setInVideoFormat(std::string);
  void setOutVideoFormat(std::string);
  Video getOutVid();
  Video getInVid();
  void addEdit(Edit* Edit);
  void render();
};

#endif
