#ifndef CONVERT_HPP
# define CONVERT_HPP

#include <iostream>
#include <string>

#include "Edit.hpp"

class Convert : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;
  
public:
  Convert();
  ~Convert();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;
};

#endif