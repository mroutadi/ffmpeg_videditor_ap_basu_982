#ifndef RESOLUTION_HPP
# define RESOLUTION_HPP

#include <iostream>
#include <string>

#include "Edit.hpp"

class Resolution : public Edit
{
private:
  Command command;
  Video inputVideo;
  Video outputVideo;

  std::string resolution;
  
public:
  Resolution();
  ~Resolution();

  void setInputVideo(Video Inputvideo);
  void setOutputVideo(Video Outputvideo);

  void setResolution(std::string ResolutionsENumMember);

  void Do(Video Inputvideo, Video Output) override;
  void setCommand(std::string command);
  std::string getEditCommand() override;
};

#endif